import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Column {

    spacing: units.gu(1)
    anchors {
        margins: units.gu(2)        
    }
    width: parent.width

    property alias header: listHeader.text

    property var transportMode: {
        "all":  transportModeAllCheck.checked,
        "bus":  transportModeBusCheck.checked,
        "train": transportModeTrainCheck.checked,
        "metro": transportModeMetroCheck.checked,
        "tram": transportModeTramCheck.checked,
        "service_line": transportModeServiceCheck.checked,
        "u_line": transportModeUlineCheck.checked,
        "ferry": transportModeFerryCheck.checked,
        "walk": transportModeWalkCheck.checked
    }

    ListItem.Header {
        id: listHeader
    }

    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeAllCheck
            checked: true
            onCheckedChanged: {
                if (transportModeAllCheck.checked === true) {
                    transportModeAllCheck.checked = true;
                    transportModeBusCheck.checked = true;
                    transportModeTrainCheck.checked = true;
                    transportModeMetroCheck.checked = true;
                    transportModeTramCheck.checked = true;
                    transportModeServiceCheck.checked = true;
                    transportModeUlineCheck.checked = true;
                    transportModeFerryCheck.checked = true;
                    transportModeWalkCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeAllLabel
            text: "All"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeBusCheck
            checked: true
            onCheckedChanged: {
                if (transportModeBusCheck.checked) {
                    transportModeWalkCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeBusLabel
            text: "Bus"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeTrainCheck
            checked: true
            onCheckedChanged: {
                if (transportModeTrainCheck.checked) {
                    transportModeWalkCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeTrainLabel
            text: "Train"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeMetroCheck
            checked: true
            onCheckedChanged: {
                if (transportModeMetroCheck.checked) {
                    transportModeWalkCheck.checked = false;
                }
        }
        }
        Label {
            id: transportModeMetroLabel
            text: "Subway"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeTramCheck
            checked: true
            onCheckedChanged: {
                if (transportModeTramCheck.checked) {
                    transportModeWalkCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeTramLabel
            text: "Tram"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeServiceCheck
            checked: true
            onCheckedChanged: {
                if (transportModeServiceCheck.checked) {
                    transportModeWalkCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeServiceLabel
            text: "Service line"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeUlineCheck
            checked: true
            onCheckedChanged: {
                if (transportModeUlineCheck.checked) {
                    transportModeWalkCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeUlineLabel
            text: "U-line"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeFerryCheck
            checked: true
            onCheckedChanged: {
                if (transportModeFerryCheck.checked) {
                    transportModeWalkCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeFerryLabel
            text: "Ferry"
            fontSize: "medium"
        }
    }
    Row {
        spacing: units.gu(1)

        CheckBox {
            id: transportModeWalkCheck
            checked: false
            onCheckedChanged: {
                if (transportModeWalkCheck.checked === true) {
                    transportModeAllCheck.checked = false;
                    transportModeBusCheck.checked = false;
                    transportModeTrainCheck.checked = false;
                    transportModeMetroCheck.checked = false;
                    transportModeTramCheck.checked = false;
                    transportModeServiceCheck.checked = false;
                    transportModeUlineCheck.checked = false;
                    transportModeFerryCheck.checked = false;
                }
            }
        }
        Label {
            id: transportModeWalkLabel
            text: "Only walking"
            fontSize: "medium"
        }
    }

}
