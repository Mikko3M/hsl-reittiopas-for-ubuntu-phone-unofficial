function getLocationList(listModelObject, searchInput, locationListpopOver) {

    var request = new XMLHttpRequest();
    var departureUrl  = "http://api.reittiopas.fi/hsl/prod/?user="+apiUsername+"&pass="+apiPassword+"&format=json&request=geocode&key="+encodeURIComponent(searchInput.text);
    
    request.open("GET", departureUrl, true);
    request.setRequestHeader("X-Accept", "application/json");
    request.onreadystatechange = function() {

        if (request.readyState === XMLHttpRequest.DONE) {
            var haltPopup = false;
            var json_result = "";
        try {
            json_result = JSON.parse(request.responseText);
        } catch (err) {
            haltPopup = true;
        }

        for (var i=0;i<json_result.length;i++) {

            var name    = json_result[i].name;
            var coords  = json_result[i].coords;
            var city    = json_result[i].city;
            var nameCt  = "";

            if (json_result[i].locType === "address") {

            if (json_result[i].details.houseNumber !== undefined) {
                nameCt = name+", "+json_result[i].details.houseNumber.toString()+", "+city;
            }

            } else if (json_result[i].locType === "stop") {
                nameCt = name+", "+city+" "+"("+"Stop "+json_result[i].details.address+" "+json_result[i].details.shortCode.toString()+")";
            } else {
                nameCt = name+", "+city;
            }

            listModelObject.append({"name":nameCt,"coords":coords,"city":city});
        }

        if (Boolean(haltPopup) === false) {
            PopupUtils.open(locationListpopOver, searchInput);
        }
            activityIndicator.running = false;
        }

    };
    request.send();

}

function getRouteList(listModelObject) {

    var transportTypes = "";
    if (complexSearchOptions.transport["all"]) {
        transportTypes = "all";
    }
    else if (complexSearchOptions.transport["walk"]) {
        transportTypes = "walk";
    }
    else {
        if (complexSearchOptions.transport["bus"]) {
            transportTypes = transportTypes + "|" + "bus";
        }
        if (complexSearchOptions.transport["ferry"]) {
            transportTypes = transportTypes + "|" + "ferry";
        }
        if (complexSearchOptions.transport["metro"]) {
            transportTypes = transportTypes + "|" + "metro";
        }
        if (complexSearchOptions.transport["u_line"]) {
            transportTypes = transportTypes + "|" + "uline";
        }
        if (complexSearchOptions.transport["service_line"]) {
            transportTypes = transportTypes + "|" + "service";
        }
        if (complexSearchOptions.transport["tram"]) {
            transportTypes = transportTypes + "|" + "tram";
        }
        if (complexSearchOptions.transport["train"]) {
            transportTypes = transportTypes + "|" + "train";
        }
    }

    if (transportTypes.substring(0,1) === "|") {
        transportTypes = transportTypes.substring(1,transportTypes.length);
    }

    var request = new XMLHttpRequest();
    var routeUrl  = "http://api.reittiopas.fi/hsl/prod/?user="+apiUsername+"&pass="+apiPassword+"&format=json&request=route&from="+coordinateItem.startingPoint+"&to="+coordinateItem.arrivalPoint+"&transport_types="+transportTypes+"&walk_speed="+walkingSpeedSlider.value;

    request.open("GET", routeUrl, true);
    request.setRequestHeader("X-Accept", "application/json");
    request.onreadystatechange = function() {


        if (request.readyState === XMLHttpRequest.DONE) {
            var resultRoutes = JSON.parse(request.responseText)
            for (var i=0;i<resultRoutes.length;i++) {
                var routes = resultRoutes[i];
                for (var j=0;j<routes.length;j++) {

                    var duration = routes[j].duration / 60;
                    duration = duration.toString()+" min";

                    var lengthKm = (routes[j].length / 1000).toString()+" km";

                    var firstLocLength = routes[j].legs[0].locs.length-1;

                    var departureTime = routes[j].legs[0].locs[0].depTime.toString().substring(8,12);
                    departureTime = departureTime.substring(0,2)+":"+departureTime.substring(2,4);

                    var departureTimeFromStop = routes[j].legs[0].locs[firstLocLength].depTime.toString().substring(8,12);
                    departureTimeFromStop = departureTimeFromStop.substring(0,2)+":"+departureTimeFromStop.substring(2,4);

                    var legsLength = routes[j].legs.length-1;
                    var locsLength = routes[j].legs[legsLength].locs.length-1;

                    var arrivalTime = routes[j].legs[legsLength].locs[locsLength].arrTime.toString().substring(8,12);
                    arrivalTime = arrivalTime.substring(0,2)+":"+arrivalTime.substring(2,4);

                    var walkingDistanceNumeric = 0;
                    var transportCode = "";
                    var type = "walk";

                    if (legsLength === 0) {
                        if (routes[j].legs[0].type === "walk") {
                            walkingDistanceNumeric = walkingDistanceNumeric+routes[j].legs[0].length;
                            transportCode = "walk";
                        } else {
                            transportCode = routes[j].legs[0].code.substring(1,5);
                            type = routes[j].legs[0].type;
                        }
                        departureTimeFromStop = departureTime;
                    }
                    else {
                        for (var l=0;l<legsLength;l++) {
                            if (routes[j].legs[l].type === "walk") {
                                walkingDistanceNumeric = walkingDistanceNumeric+routes[j].legs[l].length;
                            } else {
                                transportCode = routes[j].legs[l].code.substring(1,5);
                                type = routes[j].legs[l].type;
                            }
                        }
                    }

                    if (transportCode.substring(0,2)=="00") {
                        transportCode = transportCode.substring(2,4);
                    } else if (transportCode.substring(0,1)=="0") {
                        transportCode = transportCode.substring(1,4);
                    }

                    if (type === "6") {
                        transportCode = "M";
                    }


                    walkingDistanceNumeric = walkingDistanceNumeric / 1000;
                    var walkingDistance = Math.round(walkingDistanceNumeric * 100) / 100;

                    listModelObject.append({"duration":duration,"type":type,"length":lengthKm.toString(),"departureTime":departureTime,"departureTimeFromStop":departureTimeFromStop,"arrivalTime":arrivalTime,"walkingDistance":walkingDistance,"transportCode":transportCode,"legs":routes[j].legs});

                }
            }
        }
        activityIndicator.running = false;
    };
    request.send();
}

function getSingleRoute(listModelObject, index) {

    var singleRoute = routeModel.get(index).legs;
    for (var i=0; i<singleRoute.count; i++) {

        var duration = singleRoute.get(i).duration / 60;
        duration = Math.round(duration * 100) / 100;
        duration = duration.toString()+" min";

        var type = singleRoute.get(i).type;

        var locs = singleRoute.get(i).locs.count-1;

        if (locs === 0) {
            locs = 1;
        }


        var firstLocItem = singleRoute.get(i).locs.get(0);
        var lastLocItem = singleRoute.get(i).locs.get(locs);

        var startName = "";
        if (firstLocItem.name === undefined || firstLocItem.name === "") {
            startName = departurePlace.text;
        } else {
            startName = firstLocItem.name;
            if (firstLocItem.shortCode !== undefined) {
                startName = firstLocItem.name+" (Stop "+firstLocItem.shortCode.toString()+")";
            }

        }

        var stopName = "";
        if (lastLocItem.name === undefined || lastLocItem.name === "") {
            stopName = arrivalPlace.text;
        } else {
            stopName = lastLocItem.name;
            if (lastLocItem.shortCode !== undefined) {
                stopName = lastLocItem.name+" (Stop "+lastLocItem.shortCode.toString()+")";
            }
        }

        var departureTimeFromStop = firstLocItem.depTime.toString().substring(8,12);
        departureTimeFromStop = departureTimeFromStop.substring(0,2)+":"+departureTimeFromStop.substring(2,4);

        var transportCode = "";
        if (singleRoute.get(i).type !== "walk") {
            transportCode = singleRoute.get(i).code.substring(1,5);
        } else {
            transportCode = "walk";
        }

        if (transportCode.substring(0,2)=="00") {
            transportCode = transportCode.substring(2,4);
        } else if (transportCode.substring(0,1)=="0") {
            transportCode = transportCode.substring(1,4);
        }

        if (singleRoute.get(i).type === "6") {
            transportCode = "M";
        }

        listModelObject.append({"duration":duration,"type":singleRoute.get(i).type,"startName":startName,"stopName":stopName,"departureTimeFromStop":departureTimeFromStop,"code":transportCode,"type":singleRoute.get(i).type});
    }

}
