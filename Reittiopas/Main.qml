import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Pickers 1.0
import QtQuick.Layouts 1.1

import "ui"
import "js/library.js" as HslApi


/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "reittiopas.mikko3m"

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    //automaticOrientation: true


    width: units.gu(50)
    height: units.gu(75)

    property string apiUsername : ""
    property string apiPassword : ""

    PageStack {
        id: pageStack

        Component.onCompleted: push(mainPage)

        Page {

            id: mainPage
            visible: false

            title: i18n.tr("HSL reittiopas (Unofficial)")

            head.actions: [
                 Action {
                     iconName: "settings"
                     text: i18n.tr("Settings")
                     onTriggered: {
                        pageStack.push(complexSearchOptions);
                     }
                 }
             ]


            width: parent.width

            property var typeToColor: {
                1:"#007AC9",
                2:"#00985F",
                3:"#007AC9",
                4:"#007AC9",
                5:"#007AC9",
                6:"#FF6319",
                7:"#00B9E4",
                8:"#007AC9",
                12:"#8C4799",
                21:"#F092CD",
                22:"#007AC9",
                23:"#F092CD",
                24:"#F092CD",
                25:"#007AC9",
                36:"#007AC9",
                38:"#007AC9",
                39:"#007AC9",
                "walk": "#F4DEEA"
            }

            ListModel {
                id: departureModel
            }

            ListModel {
                id: arrivalModel
            }

            ListModel {
                id: routeModel
            }

            Item {
                id: coordinateItem
                property string startingPoint: ""
                property string arrivalPoint: ""
            }

            Column {
                spacing: units.gu(1)
                anchors {
                    margins: units.gu(2)
                    fill: parent
                }

                Item {
                    ActivityIndicator {
                        id: activityIndicator
                        y:units.gu(60)
                        x:units.gu(20)
                        running: false
                    }
                }

                TextField {
                    id: departurePlace
                    width: parent.width
                    placeholderText: i18n.tr("Place of departure")
                    onAccepted: {
                         activityIndicator.running = true;
                         departureModel.clear();
                         HslApi.getLocationList(departureModel, departurePlace, popoverDepartureComponent);
                    }
                }
                TextField {
                    id: arrivalPlace
                    width: parent.width
                    placeholderText: i18n.tr("Place of arrival")
                    onAccepted: {
                         activityIndicator.running = true;
                         arrivalModel.clear();
                         HslApi.getLocationList(arrivalModel, arrivalPlace, popoverArrivalComponent);
                    }
                }
                Component {
                    id: popoverDepartureComponent
                    Popover {
                        id: popoverDeparturePlace
                        Column {
                            id: containerLayout

                            anchors {
                                left: parent.left
                                top: parent.top
                                right: parent.right
                            }
                            height: mainPage.height
                            ListItem.Header { text: "Pick a starting point" }
                            UbuntuListView {
                                clip: true
                                width: parent.width
                                height: units.gu(24)
                                model: departureModel

                                delegate: ListItem.Standard {
                                    text: name
                                    onClicked: {
                                        departurePlace.text = name;
                                        coordinateItem.startingPoint = coords;
                                        PopupUtils.close(popoverDeparturePlace);
                                     }
                                }
                            }
                        }
                    }
                }
                Component {
                    id: popoverArrivalComponent
                    Popover {
                        id: popoverArrivalPlace
                        Column {
                            id: containerLayout

                            anchors {
                                left: parent.left
                                top: parent.top
                                right: parent.right
                            }

                            ListItem.Header { text: "Pick a place to go" }
                            UbuntuListView {
                                clip: true
                                width: parent.width
                                height: units.gu(24)
                                model: arrivalModel

                                delegate: ListItem.Standard {
                                    text: name
                                    onClicked: {
                                        arrivalPlace.text = name;
                                        coordinateItem.arrivalPoint = coords;
                                        PopupUtils.close(popoverArrivalPlace);
                                     }
                                }
                            }
                        }
                    }
                }

                Button {
                    objectName: "button"
                    width: parent.width
                    id: searchRouteButton
                    text: i18n.tr("Find route!")

                    onClicked: {
                        activityIndicator.running = true;
                        routeModel.clear();
                        singleRouteModel.clear();
                        HslApi.getRouteList(routeModel);
                    }
                }

                UbuntuListView {

                    anchors { left: parent.left; right: parent.right }
                    height: units.gu(75)
                    model: routeModel
                    id: ubuntuListView
                    clip: true

                    delegate: ListItemLayout {
                        id: layout

                        title {
                            text: departureTime+ " ("+departureTimeFromStop+")"
                            color: UbuntuColors.orange
                        }

                        subtitle.text: "Walking: "+walkingDistance+" "+"km"
                        //summary.text: "summary of route"

                        Rectangle {
                            id: rec
                            SlotsLayout.position: SlotsLayout.Leading
                            color: mainPage.typeToColor[type]
                            radius: 5

                            height: units.gu(6)
                            width: height
                            Text {
                                text: transportCode
                                id: recText
                                anchors.horizontalCenter: rec.horizontalCenter
                                anchors.verticalCenter: rec.verticalCenter
                            }
                        }
                        Label {
                            id: firstLabel
                            SlotsLayout.position: SlotsLayout.Trailing-1;
                            text: arrivalTime
                            fontSize: "medium"
                        }
                        Icon {
                            name: "go-to"
                            SlotsLayout.position: SlotsLayout.Trailing;
                            width: units.gu(5)
                            height: units.gu(10)

                            MouseArea {
                                width: parent.width
                                height: parent.height

                                onClicked: {
                                    routePage.index = -1;
                                    pageStack.push(routePage, {index:index});
                                }
                            }

                         }

                    }
                }
            }
        }

        Page {
            id: complexSearchOptions

            visible: false

            title: i18n.tr("HSL reittiopas (Unofficial)")

            width: parent.width

            property var transport: transportationModeSelection.transportMode
            Column {
                anchors {
                    margins: units.gu(2)
                    fill: parent
                }
                TransportationMode {
                    id: transportationModeSelection
                    header: i18n.tr("Choose transport methods")
                    height: units.gu(30)
                }


                ListItem.Header {
                    id: walkingSpeedSliderHeader
                    anchors.top: transportationModeSelection.bottom
                    anchors.topMargin: units.gu(2)
                    text: i18n.tr("Set walking speed (m/min)")
                }

                Item {
                    anchors.top: walkingSpeedSliderHeader.bottom
                    Slider {
                        id: walkingSpeedSlider
                        function formatValue(v) { return v.toFixed(2) }
                        minimumValue: 1
                        maximumValue: 500
                        stepSize: 1
                        value: 70
                        live: true
                    }
                }
            }

        }

        Page {
            id:routePage
            visible: false

            title: i18n.tr("Route")

            property int index: -1

            ListModel {
                id: singleRouteModel
            }

            onIndexChanged: {

                singleRouteModel.clear();

                if (index >= 0) {
                    HslApi.getSingleRoute(singleRouteModel, index)
                }
            }

            Column {
                spacing: units.gu(1)
                anchors {
                    margins: units.gu(2)
                    fill: parent
                }

                UbuntuListView {
                    anchors { left: parent.left; right: parent.right }
                    height: units.gu(75)
                    model: singleRouteModel
                    id: ubuntuListViewRoute
                    clip: true

                    delegate: ListItemLayout {
                        id: singleRouteLayout

                        title {
                            text: departureTimeFromStop
                            color: UbuntuColors.orange
                        }

                        subtitle.text: startName
                        summary.text: stopName

                        Rectangle {
                            id: routeRect
                            SlotsLayout.position: SlotsLayout.Leading
                            color: mainPage.typeToColor[type]
                            radius: 5

                            height: units.gu(6)
                            width: height
                            Text {
                                text: code
                                id: routeTransportCode
                                anchors.horizontalCenter: routeRect.horizontalCenter
                                anchors.verticalCenter: routeRect.verticalCenter
                            }
                        }
                    }
                }
            }
        }
    }
}
